import { createRouter, createWebHistory, Router, RouteRecordRaw } from 'vue-router'

const routes:Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/connexion',
    name: 'Connexion',
    component: () => import('../views/Connexion.vue')
  },
  {
    path: '/productCatalog',
    name: 'ProductCatalog',
    component: () => import('../views/ProductCatalog.vue')
  },
  {
    path: '/productDetail/:id',
    name: 'ProductDetail',
    component: () => import('../views/ProductDetail.vue')
  },
  {
    path: '/payment',
    name: 'Payment',
    component: () => import('../views/Payment.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  }
]

const router:Router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
