import express from 'express'
import history from 'connect-history-api-fallback'
import mysql from 'mysql2'
import sql from './sql'
import api from './api/api'

export default function server(port:number, option:mysql.ConnectionOptions) {

    const app:express.Application = express()
    app.use(express.json())
    app.use(express.urlencoded({ extended: false }))
    app.use(function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', '*');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        next();
    })

    sql.connect(option)

    app.use('/api', api)
    app.use(express.static('dist'))
    app.use('/assets', express.static('src/assets'))
    app.use(history({index: '/dist/index.html'}))

    app.listen(port, function () {
        console.log("Server listen on port : http://localhost:" + port + "/")
    })
}
