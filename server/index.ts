import server from './express'

// parametre du serveur et du compte client 
server(
    3000, // Ne pas changer le port 3000
    {
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'st2awd-projet'
    }
)
