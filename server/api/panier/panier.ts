import express, { Router } from 'express'
const router:Router = express.Router()
import sql from '../../sql'
import {Panier} from 'types/Panier'
import { User } from 'types/User'
import { PanierItem } from 'types/PanierItem'
import { Error } from '../../../types/Error'



// <host>/api/panier/order <-->  {panier:Panier}
router.post('/order', async (req, res) => {

    const panier:Panier=req.body.panier
    const user:User=panier.user
    const date:Date=new Date(Date.now())
    const sqlDate:string = date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0]

    console.log(panier.panier.length)

    for ( let i = 0 ; i < panier.panier.length ; i++) {
        const element:PanierItem = panier.panier[i]
        console.dir(element)
        const insertQuery: any = await sql.query(
            'INSERT INTO orders (user_id, date, product_id, quantity) VALUES (?, ?, ?, ?)',
            [user.id, sqlDate, element.item.id, element.quantite]
        )

        if (insertQuery.error){
            const err:Error = {datatime : new Date().toISOString(), source : "/api/panier/order" , message : insertQuery.error.sqlMessage}
            return res.status(408).json({error:err, panier:null})
        }

    }

    return res.status(200).json({error:null, panier:panier})
})

export default router