import express, { Router } from 'express'
const router:Router = express.Router()
import sql from '../../sql'
import bcryptjs from 'bcryptjs'
import { User } from '../../../types/User'
import { Error } from '../../../types/Error'
const salt:string = bcryptjs.genSaltSync(10);


// <host>/api/user/login <-->  {email:String, password:String}
router.post('/login', async (req, res) => {

    const email:string = req.body.email
    const password:string = req.body.password
  
    const result:any = await sql.query(     
      'SELECT * FROM users WHERE email = ?',
      [email]
    )

    if(result.error) {
      const err:Error = {datatime : new Date().toISOString(), source : "/api/user/login" , message : result.error.sqlMessage}
      return res.status(408).json({error : err, user:null})
    }
  
    if (result.results.length<=0) {
      const err:Error = {datatime : new Date().toISOString(), source : "/api/user/login" , message : "User doesn't exist"}
      return res.status(200).json({error : err, user:null})
    }

    const hash:string = result.results[0].hash

    if (await bcryptjs.compare(password, hash)){

          const user:User={
            id:result.results[0].id,
            email:email,
            information_personnelle: {
              nom: result.results[0].last_name,
              prenom: result.results[0].first_name,
              telephone: result.results[0].tel,
              pays: result.results[0].country,
              adresse: result.results[0].address,
          }
      }

    return res.status(200).json({error : null, user:user})
    }
 
    else {
      const err:Error = {datatime : new Date().toISOString(), source : "/api/user/login" , message : "Wrong password"}
      return res.status(200).json({error : err, user:null})
    }

})

// <host>/api/user/register <--> {email:String, password:String}
router.post('/register', async (req, res) => {

    const email:string = req.body.email
    const password:string = req.body.password
  
    const result:any = await sql.query(
      'SELECT * FROM users WHERE email = ?',
      [email]
    )


    if(result.error){
      const err:Error = {datatime : new Date().toISOString(), source : "/api/user/register" , message : result.error.sqlMessage}
      return res.status(408).json({error : err, user:null})
    }


    if (result.results.length > 0) {
      const err:Error = {datatime : new Date().toISOString(), source : "/api/user/register" , message : "User already exists"}
      return res.status(200).json({error : err, user:null})
    }
  
    const hash:string = await bcryptjs.hash(password, salt)
    const userID:string = await bcryptjs.hash(Date.now().toString(), salt)

    const insertQuery:any = await sql.query(
      'INSERT INTO users (email, hash, ID) VALUES (?, ?, ?)',
      [email, hash, userID]
    )

    if(insertQuery.error) {
      const err:Error = {datatime : new Date().toISOString(), source : "/api/user/register" , message : insertQuery.error.sqlMessage}
      return res.status(408).json({error : err, user:null})
    }

    const newUser:User = {
      id: userID,
      email: email
    }

    return res.status(200).json({error : null, user:newUser})
})

// <host>/api/user/updateInfo <--> {userID:String, email:String, firstName:String, lastName:String, tel:String, country:String, address:String}
router.put('/updateInfo', async (req, res) =>{

  if(!req.body.userID || !req.body.email) {
    const err:Error = {datatime : new Date().toISOString(), source : "/api/user/updateInfo" , message : "Bad request"}
    return res.status(400).json({error : err, user:null})
  }

  const result:any = await sql.query(
    'SELECT * FROM users WHERE id = ?',
    [req.body.userID]
  )

  if(result.error) {
    const err:Error = {datatime : new Date().toISOString(), source : "/api/user/updateInfo" , message : result.error.sqlMessage}
    return res.status(408).json({error : err, user:null})
  }
  
  if(result.results.length <= 0 ) {
    const err:Error = {datatime : new Date().toISOString(), source : "/api/user/updateInfo" , message : "User not found"}
    return res.status(200).json({error : err, user:null})
  }

  const oldUser:any = result.results[0]

  const userID:string = req.body.userID
  const firstName:string = req.body.firstName ? req.body.firstName : oldUser.first_name
  const lastName:string = req.body.lastName ? req.body.lastName : oldUser.last_name
  const tel:string = req.body.tel ? req.body.tel : oldUser.tel
  const country:string = req.body.country ? req.body.country : oldUser.country
  const address:string = req.body.address ? req.body.address : oldUser.address

  const updateQuery:any = await sql.query(
    'UPDATE users SET first_name=?, last_name=?, tel=?, country=?, address=? WHERE ID=?',
    [firstName, lastName, tel, country, address, userID]
  )

  if(updateQuery.error) {
    const err:Error = {datatime : new Date().toISOString(), source : "/api/user/updateInfo" , message : updateQuery.error.sqlMessage}
    return res.status(408).json({error : err, user:null})
  }

  const newUser:User = {
    id: userID,
    email: req.body.email,
    information_personnelle: {
        nom: firstName,
        prenom: lastName,
        telephone: tel,
        pays: country,
        adresse: address
    }
  }
  return res.status(200).json({error : null, user:newUser})
  
})



export default router   