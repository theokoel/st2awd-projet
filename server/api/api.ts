import express, { Router } from 'express'
const router:Router = express.Router()

import morgan from 'morgan'
router.use(morgan('combined'))

import user from './user/user'
router.use('/user', user)

import panier from './panier/panier'
router.use('/panier', panier)

import product from './product/product'
router.use('/product', product)

router.use((error:any, req:any, res:any, next:any) => {
    console.info(error, req, res, next)
})

module.exports = router
export default router