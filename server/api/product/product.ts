import express, { Router } from 'express'
const router:Router = express.Router()
import sql from '../../sql'
import { Cube } from 'types/Cube'
import { Error } from '../../../types/Error'


// <host>/api/product <-->  {}
router.get('/', async (req, res) => {
  
    const result:any = await sql.query(     
      'SELECT * FROM products'
      ,[]
    )

    if(result.error) {
      const err:Error = {datatime : new Date().toISOString(), source : "/api/product" , message : result.error.sqlMessage}
      return res.status(408).json({err : err, cubeArrray : null})
    }
    
        const cubeArrray:Cube[] = []
        result.results.forEach((item:Cube) => {
            const cube:Cube={id:item.id, name: item.name, color: item.color, price: item.price, asset: item.asset, description:item.description}
            cubeArrray.push(cube)
        });

    return res.status(200).json({error : null, cubeArray : cubeArrray})
})

// <host>/api/product/:id <-->  {}
router.get('/:id', async (req, res) => {
  
    const id = req.params.id

    const result:any = await sql.query(     
      'SELECT * FROM products WHERE id = ?'
      ,[id]
    )

    if(result.error) {
      const err:Error = {datatime : new Date().toISOString(), source : "/api/product:id", message : result.error.sqlMessage}
      return res.status(408).json({error : err, cube:null})
    }

    if (result.results.length <= 0) {
      const err:Error = {datatime : new Date().toISOString(), source : "/api/product:id" , message : "Item doesn't exist"}
      return res.status(200).json({error : err, cube:null})
    }

    const cube:Cube = result.results[0]
    
    return res.status(200).json({error : null, cube:cube})
})

export default router   