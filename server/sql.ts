import mysql from 'mysql2'

class sql {

    private static connexion:mysql.Connection

    public static connect(option:mysql.ConnectionOptions) {
        this.connexion = mysql.createConnection(option);
        this.connexion.connect()
    }

    public static query(query:string, params:Array<string|number>):Promise<unknown> {
        return new Promise(resolve => {
            this.connexion.query(query, params, (error, results, fields) => {
                resolve({error, results: results || [], fields})
            })
        })
    }
}

export default sql
