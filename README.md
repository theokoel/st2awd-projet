# st2awd-projet
## Environement
- NodeJS v14.15.3 or more
- @vue/cli v4.5.13 or more
- MySQL v5.7.24 or more
- Typescript env config in global
## Project setup
### Configuration
- Configure your database in the ./server/index.ts file (Do not change port 3000 !!!).
- import the ./st2awd-projet.sql file in your database.
### Commandes
```
npm i
```
## Run project
```
npm run serve
```
The site is available on http://localhost:8080
## Git
```
https://gitlab.com/theokoel/st2awd-projet
```
## Presentary movie
In ./st2awd-projet - video presentation.mp4