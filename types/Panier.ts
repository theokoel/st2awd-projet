import {User} from './User'
import {PanierItem} from './PanierItem'
export type Panier = {user: User, panier: PanierItem[]}
