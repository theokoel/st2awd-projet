export type User = {
    id: string,
    email: string,
    information_personnelle?: {
        nom: string,
        prenom: string,
        telephone: string,
        pays: string,
        adresse: string
    }
}